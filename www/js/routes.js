angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('club', {
    url: '/club',
    templateUrl: 'templates/club.html',
    controller: 'clubCtrl'
  })

  .state('encontrar', {
    url: '/encontrar',
    templateUrl: 'templates/encontrar.html',
    controller: 'encontrarCtrl'
  })

  .state('lecciones', {
    url: '/lecciones',
    templateUrl: 'templates/lecciones.html',
    controller: 'leccionesCtrl'
  })

  .state('signup', {
    url: '/signup',
    templateUrl: 'templates/signup.html',
    controller: 'signupCtrl'
  })

  .state('perfil', {
    url: '/perfil',
    templateUrl: 'templates/perfil.html',
    controller: 'perfilCtrl'
  })

  .state('misAmigos', {
    url: '/amigos',
    templateUrl: 'templates/misAmigos.html',
    controller: 'misAmigosCtrl'
  })

  .state('miCuenta', {
    url: '/cuenta',
    templateUrl: 'templates/miCuenta.html',
    controller: 'miCuentaCtrl'
  })

  .state('misLogros', {
    url: '/logros',
    templateUrl: 'templates/misLogros.html',
    controller: 'misLogrosCtrl'
  })

  .state('accesoRPido', {
    url: '/accesorapido',
    templateUrl: 'templates/accesoRPido.html',
    controller: 'accesoRPidoCtrl'
  })

  .state('crearClub', {
    url: '/crearclub',
    templateUrl: 'templates/crearClub.html',
    controller: 'crearClubCtrl'
  })

  .state('abecedario', {
    url: '/abecedario',
    templateUrl: 'templates/abecedario.html',
    controller: 'abecedarioCtrl'
  })

  .state('mDulo1', {
    url: '/modulo1',
    templateUrl: 'templates/mDulo1.html',
    controller: 'mDulo1Ctrl'
  })

  .state('mDulo2', {
    url: '/modulo2',
    templateUrl: 'templates/mDulo2.html',
    controller: 'mDulo2Ctrl'
  })

  .state('mDulo3', {
    url: '/modulo3',
    templateUrl: 'templates/mDulo3.html',
    controller: 'mDulo3Ctrl'
  })

  .state('a', {
    url: '/a',
    templateUrl: 'templates/a.html',
    controller: 'aCtrl'
  })

  .state('b', {
    url: '/b',
    templateUrl: 'templates/b.html',
    controller: 'bCtrl'
  })

  .state('c', {
    url: '/c',
    templateUrl: 'templates/c.html',
    controller: 'cCtrl'
  })

  .state('d', {
    url: '/d',
    templateUrl: 'templates/d.html',
    controller: 'dCtrl'
  })

  .state('e', {
    url: '/e',
    templateUrl: 'templates/e.html',
    controller: 'eCtrl'
  })

  .state('h', {
    url: '/h',
    templateUrl: 'templates/h.html',
    controller: 'hCtrl'
  })

  .state('i', {
    url: '/i',
    templateUrl: 'templates/i.html',
    controller: 'iCtrl'
  })

  .state('k', {
    url: '/k',
    templateUrl: 'templates/k.html',
    controller: 'kCtrl'
  })

  .state('l', {
    url: '/l',
    templateUrl: 'templates/l.html',
    controller: 'lCtrl'
  })

  .state('m', {
    url: '/m',
    templateUrl: 'templates/m.html',
    controller: 'mCtrl'
  })

  .state('n', {
    url: '/n',
    templateUrl: 'templates/n.html',
    controller: 'nCtrl'
  })

  .state('o', {
    url: '/o',
    templateUrl: 'templates/o.html',
    controller: 'oCtrl'
  })

  .state('p', {
    url: '/p',
    templateUrl: 'templates/p.html',
    controller: 'pCtrl'
  })

  .state('q', {
    url: '/q',
    templateUrl: 'templates/q.html',
    controller: 'qCtrl'
  })

  .state('r', {
    url: '/r',
    templateUrl: 'templates/r.html',
    controller: 'rCtrl'
  })

  .state('t', {
    url: '/t',
    templateUrl: 'templates/t.html',
    controller: 'tCtrl'
  })

  .state('u', {
    url: '/u',
    templateUrl: 'templates/u.html',
    controller: 'uCtrl'
  })

  .state('v', {
    url: '/v',
    templateUrl: 'templates/v.html',
    controller: 'vCtrl'
  })

  .state('w', {
    url: '/w',
    templateUrl: 'templates/w.html',
    controller: 'wCtrl'
  })

  .state('nombre', {
    url: '/nombre',
    templateUrl: 'templates/nombre.html',
    controller: 'nombreCtrl'
  })

  .state('yo', {
    url: '/yo',
    templateUrl: 'templates/yo.html',
    controller: 'yoCtrl'
  })

  .state('t2', {
    url: '/tu',
    templateUrl: 'templates/t2.html',
    controller: 't2Ctrl'
  })

  .state('LElla', {
    url: '/elella',
    templateUrl: 'templates/LElla.html',
    controller: 'LEllaCtrl'
  })

  .state('ellosEllas', {
    url: '/ellosellas',
    templateUrl: 'templates/ellosEllas.html',
    controller: 'ellosEllasCtrl'
  })

  .state('nosotros', {
    url: '/nosotros',
    templateUrl: 'templates/nosotros.html',
    controller: 'nosotrosCtrl'
  })

  .state('mO', {
    url: '/mio',
    templateUrl: 'templates/mO.html',
    controller: 'mOCtrl'
  })

  .state('seA', {
    url: '/seña',
    templateUrl: 'templates/seA.html',
    controller: 'seACtrl'
  })

  .state('oyente', {
    url: '/oyente',
    templateUrl: 'templates/oyente.html',
    controller: 'oyenteCtrl'
  })

  .state('sordo', {
    url: '/sordo',
    templateUrl: 'templates/sordo.html',
    controller: 'sordoCtrl'
  })

  .state('no', {
    url: '/no',
    templateUrl: 'templates/no.html',
    controller: 'noCtrl'
  })

  .state('s', {
    url: '/si',
    templateUrl: 'templates/s.html',
    controller: 'sCtrl'
  })

  .state('cambiarFoto', {
    url: '/cambiarfoto',
    templateUrl: 'templates/cambiarFoto.html',
    controller: 'cambiarFotoCtrl'
  })

  .state('invitarAmigos', {
    url: '/invitaramigos',
    templateUrl: 'templates/invitarAmigos.html',
    controller: 'invitarAmigosCtrl'
  })

  .state('lecciNCompleta', {
    url: '/leccioncompleta1',
    templateUrl: 'templates/lecciNCompleta.html',
    controller: 'lecciNCompletaCtrl'
  })

  .state('lecciNCompleta2', {
    url: '/leccioncompleta2',
    templateUrl: 'templates/lecciNCompleta2.html',
    controller: 'lecciNCompleta2Ctrl'
  })

  .state('lecciNCompleta3', {
    url: '/page257',
    templateUrl: 'templates/lecciNCompleta3.html',
    controller: 'lecciNCompleta3Ctrl'
  })

  .state('listaDeAmigos', {
    url: '/listaamigos',
    templateUrl: 'templates/listaDeAmigos.html',
    controller: 'listaDeAmigosCtrl'
  })

  .state('misClubes', {
    url: '/misclubes',
    templateUrl: 'templates/misClubes.html',
    controller: 'misClubesCtrl'
  })

  .state('page49', {
    url: '/ellosellasimagen',
    templateUrl: 'templates/page49.html',
    controller: 'page49Ctrl'
  })

  .state('page51', {
    url: '/nosotrosimagen',
    templateUrl: 'templates/page51.html',
    controller: 'page51Ctrl'
  })

  .state('page50', {
    url: '/ellosellastexto',
    templateUrl: 'templates/page50.html',
    controller: 'page50Ctrl'
  })

  .state('page52', {
    url: '/nosotrostexto',
    templateUrl: 'templates/page52.html',
    controller: 'page52Ctrl'
  })

  .state('adivinaLaSeA', {
    url: '/adivina_1',
    templateUrl: 'templates/adivinaLaSeA.html',
    controller: 'adivinaLaSeACtrl'
  })

  .state('adivinaLaSeA2', {
    url: '/adivina_2',
    templateUrl: 'templates/adivinaLaSeA2.html',
    controller: 'adivinaLaSeA2Ctrl'
  })

  .state('adivinaLaSeA3', {
    url: '/adivina_3',
    templateUrl: 'templates/adivinaLaSeA3.html',
    controller: 'adivinaLaSeA3Ctrl'
  })

  .state('adivinaLaSeA4', {
    url: '/adivina_4',
    templateUrl: 'templates/adivinaLaSeA4.html',
    controller: 'adivinaLaSeA4Ctrl'
  })

  .state('adivinaLaSeA5', {
    url: '/adivina_5',
    templateUrl: 'templates/adivinaLaSeA5.html',
    controller: 'adivinaLaSeA5Ctrl'
  })

  .state('adivinaLaSeA6', {
    url: '/adivina_6',
    templateUrl: 'templates/adivinaLaSeA6.html',
    controller: 'adivinaLaSeA6Ctrl'
  })

  .state('adivinaLaSeA7', {
    url: '/adivina_7',
    templateUrl: 'templates/adivinaLaSeA7.html',
    controller: 'adivinaLaSeA7Ctrl'
  })

  .state('adivinaLaSeA8', {
    url: '/adivina_8',
    templateUrl: 'templates/adivinaLaSeA8.html',
    controller: 'adivinaLaSeA8Ctrl'
  })

  .state('adivinaLaSeA9', {
    url: '/adivina_9',
    templateUrl: 'templates/adivinaLaSeA9.html',
    controller: 'adivinaLaSeA9Ctrl'
  })

  .state('adivinaLaSeA10', {
    url: '/adivina_10',
    templateUrl: 'templates/adivinaLaSeA10.html',
    controller: 'adivinaLaSeA10Ctrl'
  })

  .state('adivinaLaSeA11', {
    url: '/adivina_11',
    templateUrl: 'templates/adivinaLaSeA11.html',
    controller: 'adivinaLaSeA11Ctrl'
  })

  .state('adivinaLaSeA12', {
    url: '/adivina_12',
    templateUrl: 'templates/adivinaLaSeA12.html',
    controller: 'adivinaLaSeA12Ctrl'
  })

  .state('adivinaLaSeA13', {
    url: '/adivina_13',
    templateUrl: 'templates/adivinaLaSeA13.html',
    controller: 'adivinaLaSeA13Ctrl'
  })

  .state('ganaste', {
    url: '/ganar',
    templateUrl: 'templates/ganaste.html',
    controller: 'ganasteCtrl'
  })

  .state('ganaste2', {
    url: '/ganar2',
    templateUrl: 'templates/ganaste2.html',
    controller: 'ganaste2Ctrl'
  })

  .state('ganaste3', {
    url: '/ganar3',
    templateUrl: 'templates/ganaste3.html',
    controller: 'ganaste3Ctrl'
  })

  .state('ganaste4', {
    url: '/ganar_4',
    templateUrl: 'templates/ganaste4.html',
    controller: 'ganaste4Ctrl'
  })

  .state('ganaste5', {
    url: '/ganar_5',
    templateUrl: 'templates/ganaste5.html',
    controller: 'ganaste5Ctrl'
  })

  .state('ganaste6', {
    url: '/ganar_6',
    templateUrl: 'templates/ganaste6.html',
    controller: 'ganaste6Ctrl'
  })

  .state('ganaste7', {
    url: '/ganar7',
    templateUrl: 'templates/ganaste7.html',
    controller: 'ganaste7Ctrl'
  })

  .state('ganaste8', {
    url: '/ganar8',
    templateUrl: 'templates/ganaste8.html',
    controller: 'ganaste8Ctrl'
  })

  .state('ganaste9', {
    url: '/ganar9',
    templateUrl: 'templates/ganaste9.html',
    controller: 'ganaste9Ctrl'
  })

  .state('ganaste10', {
    url: '/ganar10',
    templateUrl: 'templates/ganaste10.html',
    controller: 'ganaste10Ctrl'
  })

  .state('ganaste11', {
    url: '/ganar11',
    templateUrl: 'templates/ganaste11.html',
    controller: 'ganaste11Ctrl'
  })

  .state('ganaste12', {
    url: '/ganar12',
    templateUrl: 'templates/ganaste12.html',
    controller: 'ganaste12Ctrl'
  })

  .state('ganaste13', {
    url: '/ganar13',
    templateUrl: 'templates/ganaste13.html',
    controller: 'ganaste13Ctrl'
  })

  .state('perdiste', {
    url: '/perdiste',
    templateUrl: 'templates/perdiste.html',
    controller: 'perdisteCtrl'
  })

  .state('perdiste2', {
    url: '/perder2',
    templateUrl: 'templates/perdiste2.html',
    controller: 'perdiste2Ctrl'
  })

  .state('perdiste3', {
    url: '/perder3',
    templateUrl: 'templates/perdiste3.html',
    controller: 'perdiste3Ctrl'
  })

  .state('perdiste4', {
    url: '/perder4',
    templateUrl: 'templates/perdiste4.html',
    controller: 'perdiste4Ctrl'
  })

  .state('perdiste5', {
    url: '/perder5',
    templateUrl: 'templates/perdiste5.html',
    controller: 'perdiste5Ctrl'
  })

  .state('perdiste6', {
    url: '/perder6',
    templateUrl: 'templates/perdiste6.html',
    controller: 'perdiste6Ctrl'
  })

  .state('perdiste7', {
    url: '/perder7',
    templateUrl: 'templates/perdiste7.html',
    controller: 'perdiste7Ctrl'
  })

  .state('perdiste8', {
    url: '/perder8',
    templateUrl: 'templates/perdiste8.html',
    controller: 'perdiste8Ctrl'
  })

  .state('perdiste9', {
    url: '/perder9',
    templateUrl: 'templates/perdiste9.html',
    controller: 'perdiste9Ctrl'
  })

  .state('perdiste10', {
    url: '/perder10',
    templateUrl: 'templates/perdiste10.html',
    controller: 'perdiste10Ctrl'
  })

  .state('perdiste11', {
    url: '/perder11',
    templateUrl: 'templates/perdiste11.html',
    controller: 'perdiste11Ctrl'
  })

  .state('perdiste12', {
    url: '/perder12',
    templateUrl: 'templates/perdiste12.html',
    controller: 'perdiste12Ctrl'
  })

  .state('perdiste13', {
    url: '/perder13',
    templateUrl: 'templates/perdiste13.html',
    controller: 'perdiste13Ctrl'
  })

  .state('gracias', {
    url: '/gracias',
    templateUrl: 'templates/gracias.html',
    controller: 'graciasCtrl'
  })

  .state('bienvenido', {
    url: '/bienvenido',
    templateUrl: 'templates/bienvenido.html',
    controller: 'bienvenidoCtrl'
  })

  .state('buenasNoches', {
    url: '/bnoches',
    templateUrl: 'templates/buenasNoches.html',
    controller: 'buenasNochesCtrl'
  })

  .state('buenasTardes', {
    url: '/btardes',
    templateUrl: 'templates/buenasTardes.html',
    controller: 'buenasTardesCtrl'
  })

  .state('buenosDAs', {
    url: '/bdias',
    templateUrl: 'templates/buenosDAs.html',
    controller: 'buenosDAsCtrl'
  })

  .state('CMoEst', {
    url: '/comoesta',
    templateUrl: 'templates/CMoEst.html',
    controller: 'CMoEstCtrl'
  })

  .state('conMuchoGusto', {
    url: '/congusto',
    templateUrl: 'templates/conMuchoGusto.html',
    controller: 'conMuchoGustoCtrl'
  })

  .state('adiS', {
    url: '/adios',
    templateUrl: 'templates/adiS.html',
    controller: 'adiSCtrl'
  })

  .state('hola', {
    url: '/hola',
    templateUrl: 'templates/hola.html',
    controller: 'holaCtrl'
  })

  .state('deNada', {
    url: '/denada',
    templateUrl: 'templates/deNada.html',
    controller: 'deNadaCtrl'
  })

  .state('felicitar', {
    url: '/felicitar',
    templateUrl: 'templates/felicitar.html',
    controller: 'felicitarCtrl'
  })

  .state('loSiento', {
    url: '/losiento',
    templateUrl: 'templates/loSiento.html',
    controller: 'loSientoCtrl'
  })

  .state('noImporta', {
    url: '/noimporta',
    templateUrl: 'templates/noImporta.html',
    controller: 'noImportaCtrl'
  })

  .state('pedirPerdon', {
    url: '/perdon',
    templateUrl: 'templates/pedirPerdon.html',
    controller: 'pedirPerdonCtrl'
  })

  .state('porFavor', {
    url: '/porfavor',
    templateUrl: 'templates/porFavor.html',
    controller: 'porFavorCtrl'
  })

  .state('presentarA', {
    url: '/page70',
    templateUrl: 'templates/presentarA.html',
    controller: 'presentarACtrl'
  })

  .state('fruta', {
    url: '/fruta',
    templateUrl: 'templates/fruta.html',
    controller: 'frutaCtrl'
  })

  .state('aguacate', {
    url: '/aguacate',
    templateUrl: 'templates/aguacate.html',
    controller: 'aguacateCtrl'
  })

  .state('banano', {
    url: '/banano',
    templateUrl: 'templates/banano.html',
    controller: 'bananoCtrl'
  })

  .state('coco', {
    url: '/coco',
    templateUrl: 'templates/coco.html',
    controller: 'cocoCtrl'
  })

  .state('durazno', {
    url: '/durazno',
    templateUrl: 'templates/durazno.html',
    controller: 'duraznoCtrl'
  })

  .state('fresa', {
    url: '/fresa',
    templateUrl: 'templates/fresa.html',
    controller: 'fresaCtrl'
  })

  .state('guayaba', {
    url: '/guayaba',
    templateUrl: 'templates/guayaba.html',
    controller: 'guayabaCtrl'
  })

  .state('limN', {
    url: '/limon',
    templateUrl: 'templates/limN.html',
    controller: 'limNCtrl'
  })

  .state('mandarina', {
    url: '/mandarina',
    templateUrl: 'templates/mandarina.html',
    controller: 'mandarinaCtrl'
  })

  .state('mango', {
    url: '/mango',
    templateUrl: 'templates/mango.html',
    controller: 'mangoCtrl'
  })

  .state('manzana', {
    url: '/manzana',
    templateUrl: 'templates/manzana.html',
    controller: 'manzanaCtrl'
  })

  .state('mora', {
    url: '/mora',
    templateUrl: 'templates/mora.html',
    controller: 'moraCtrl'
  })

  .state('naranja', {
    url: '/naranja',
    templateUrl: 'templates/naranja.html',
    controller: 'naranjaCtrl'
  })

  .state('papaya', {
    url: '/papaya',
    templateUrl: 'templates/papaya.html',
    controller: 'papayaCtrl'
  })

  .state('pera', {
    url: '/pera',
    templateUrl: 'templates/pera.html',
    controller: 'peraCtrl'
  })

  .state('piA', {
    url: '/pina',
    templateUrl: 'templates/piA.html',
    controller: 'piACtrl'
  })

  .state('uva', {
    url: '/uva',
    templateUrl: 'templates/uva.html',
    controller: 'uvaCtrl'
  })

  .state('animal', {
    url: '/animal',
    templateUrl: 'templates/animal.html',
    controller: 'animalCtrl'
  })

  .state('araA', {
    url: '/araña',
    templateUrl: 'templates/araA.html',
    controller: 'araACtrl'
  })

  .state('ballena', {
    url: '/ballena',
    templateUrl: 'templates/ballena.html',
    controller: 'ballenaCtrl'
  })

  .state('burro', {
    url: '/burro',
    templateUrl: 'templates/burro.html',
    controller: 'burroCtrl'
  })

  .state('caballo', {
    url: '/caballo',
    templateUrl: 'templates/caballo.html',
    controller: 'caballoCtrl'
  })

  .state('caracol', {
    url: '/caracol',
    templateUrl: 'templates/caracol.html',
    controller: 'caracolCtrl'
  })

  .state('cerdo', {
    url: '/cerdo',
    templateUrl: 'templates/cerdo.html',
    controller: 'cerdoCtrl'
  })

  .state('cNdor', {
    url: '/condor',
    templateUrl: 'templates/cNdor.html',
    controller: 'cNdorCtrl'
  })

  .state('conejo', {
    url: '/conejo',
    templateUrl: 'templates/conejo.html',
    controller: 'conejoCtrl'
  })

  .state('culebra', {
    url: '/culebra',
    templateUrl: 'templates/culebra.html',
    controller: 'culebraCtrl'
  })

  .state('delfN', {
    url: '/delfin',
    templateUrl: 'templates/delfN.html',
    controller: 'delfNCtrl'
  })

  .state('elefante', {
    url: '/elefante',
    templateUrl: 'templates/elefante.html',
    controller: 'elefanteCtrl'
  })

  .state('gallina', {
    url: '/gallina',
    templateUrl: 'templates/gallina.html',
    controller: 'gallinaCtrl'
  })

  .state('gallo', {
    url: '/gallo',
    templateUrl: 'templates/gallo.html',
    controller: 'galloCtrl'
  })

  .state('gato', {
    url: '/gato',
    templateUrl: 'templates/gato.html',
    controller: 'gatoCtrl'
  })

  .state('hormiga', {
    url: '/hormiga',
    templateUrl: 'templates/hormiga.html',
    controller: 'hormigaCtrl'
  })

  .state('lunes', {
    url: '/lunes',
    templateUrl: 'templates/lunes.html',
    controller: 'lunesCtrl'
  })

  .state('martes', {
    url: '/martes',
    templateUrl: 'templates/martes.html',
    controller: 'martesCtrl'
  })

  .state('miercoles', {
    url: '/miercoles',
    templateUrl: 'templates/miercoles.html',
    controller: 'miercolesCtrl'
  })

  .state('jueves', {
    url: '/jueves',
    templateUrl: 'templates/jueves.html',
    controller: 'juevesCtrl'
  })

  .state('viernes', {
    url: '/viernes',
    templateUrl: 'templates/viernes.html',
    controller: 'viernesCtrl'
  })

  .state('sBado', {
    url: '/sabado',
    templateUrl: 'templates/sBado.html',
    controller: 'sBadoCtrl'
  })

  .state('domingo', {
    url: '/domingo',
    templateUrl: 'templates/domingo.html',
    controller: 'domingoCtrl'
  })

  .state('atletismo', {
    url: '/atletismo',
    templateUrl: 'templates/atletismo.html',
    controller: 'atletismoCtrl'
  })

  .state('baloncesto', {
    url: '/baloncesto',
    templateUrl: 'templates/baloncesto.html',
    controller: 'baloncestoCtrl'
  })

  .state('billar', {
    url: '/billar',
    templateUrl: 'templates/billar.html',
    controller: 'billarCtrl'
  })

  .state('boxeo', {
    url: '/boxeo',
    templateUrl: 'templates/boxeo.html',
    controller: 'boxeoCtrl'
  })

  .state('buceo', {
    url: '/buceo',
    templateUrl: 'templates/buceo.html',
    controller: 'buceoCtrl'
  })

  .state('deporte', {
    url: '/deporte',
    templateUrl: 'templates/deporte.html',
    controller: 'deporteCtrl'
  })

  .state('fTbol', {
    url: '/futbol',
    templateUrl: 'templates/fTbol.html',
    controller: 'fTbolCtrl'
  })

  .state('patinaje', {
    url: '/patinaje',
    templateUrl: 'templates/patinaje.html',
    controller: 'patinajeCtrl'
  })

  .state('tejo', {
    url: '/tejo',
    templateUrl: 'templates/tejo.html',
    controller: 'tejoCtrl'
  })

  .state('tenis', {
    url: '/tenis',
    templateUrl: 'templates/tenis.html',
    controller: 'tenisCtrl'
  })

  .state('voleibol', {
    url: '/voleibol',
    templateUrl: 'templates/voleibol.html',
    controller: 'voleibolCtrl'
  })

  .state('ajedrez', {
    url: '/ajedrez',
    templateUrl: 'templates/ajedrez.html',
    controller: 'ajedrezCtrl'
  })

  .state('amarillo', {
    url: '/amarillo',
    templateUrl: 'templates/amarillo.html',
    controller: 'amarilloCtrl'
  })

  .state('anaranjado', {
    url: '/anaranjado',
    templateUrl: 'templates/anaranjado.html',
    controller: 'anaranjadoCtrl'
  })

  .state('azul', {
    url: '/page176',
    templateUrl: 'templates/azul.html',
    controller: 'azulCtrl'
  })

  .state('blanco', {
    url: '/blanco',
    templateUrl: 'templates/blanco.html',
    controller: 'blancoCtrl'
  })

  .state('caf', {
    url: '/café',
    templateUrl: 'templates/caf.html',
    controller: 'cafCtrl'
  })

  .state('color', {
    url: '/color',
    templateUrl: 'templates/color.html',
    controller: 'colorCtrl'
  })

  .state('gris', {
    url: '/gris',
    templateUrl: 'templates/gris.html',
    controller: 'grisCtrl'
  })

  .state('morado', {
    url: '/morado',
    templateUrl: 'templates/morado.html',
    controller: 'moradoCtrl'
  })

  .state('negro', {
    url: '/negro',
    templateUrl: 'templates/negro.html',
    controller: 'negroCtrl'
  })

  .state('rojo', {
    url: '/rojo',
    templateUrl: 'templates/rojo.html',
    controller: 'rojoCtrl'
  })

  .state('verde', {
    url: '/verde',
    templateUrl: 'templates/verde.html',
    controller: 'verdeCtrl'
  })

  .state('abogado', {
    url: '/abogado',
    templateUrl: 'templates/abogado.html',
    controller: 'abogadoCtrl'
  })

  .state('actor', {
    url: '/actor',
    templateUrl: 'templates/actor.html',
    controller: 'actorCtrl'
  })

  .state('arquitecto', {
    url: '/arquitecto',
    templateUrl: 'templates/arquitecto.html',
    controller: 'arquitectoCtrl'
  })

  .state('cajero', {
    url: '/cajero',
    templateUrl: 'templates/cajero.html',
    controller: 'cajeroCtrl'
  })

  .state('cantante', {
    url: '/cantante',
    templateUrl: 'templates/cantante.html',
    controller: 'cantanteCtrl'
  })

  .state('carpintero', {
    url: '/carpintero',
    templateUrl: 'templates/carpintero.html',
    controller: 'carpinteroCtrl'
  })

  .state('celador', {
    url: '/celador',
    templateUrl: 'templates/celador.html',
    controller: 'celadorCtrl'
  })

  .state('chef', {
    url: '/chef',
    templateUrl: 'templates/chef.html',
    controller: 'chefCtrl'
  })

  .state('cientFico', {
    url: '/cientifico',
    templateUrl: 'templates/cientFico.html',
    controller: 'cientFicoCtrl'
  })

  .state('comerciante', {
    url: '/comerciante',
    templateUrl: 'templates/comerciante.html',
    controller: 'comercianteCtrl'
  })

  .state('contador', {
    url: '/contador',
    templateUrl: 'templates/contador.html',
    controller: 'contadorCtrl'
  })

  .state('dibujante', {
    url: '/dibujante',
    templateUrl: 'templates/dibujante.html',
    controller: 'dibujanteCtrl'
  })

  .state('director', {
    url: '/director',
    templateUrl: 'templates/director.html',
    controller: 'directorCtrl'
  })

  .state('electricista', {
    url: '/electricista',
    templateUrl: 'templates/electricista.html',
    controller: 'electricistaCtrl'
  })

  .state('empleadaDomStica', {
    url: '/empleada',
    templateUrl: 'templates/empleadaDomStica.html',
    controller: 'empleadaDomSticaCtrl'
  })

  .state('enfermera', {
    url: '/enfermera',
    templateUrl: 'templates/enfermera.html',
    controller: 'enfermeraCtrl'
  })

  .state('escritor', {
    url: '/escritor',
    templateUrl: 'templates/escritor.html',
    controller: 'escritorCtrl'
  })

  .state('escultor', {
    url: '/escultor',
    templateUrl: 'templates/escultor.html',
    controller: 'escultorCtrl'
  })

  .state('filSofo', {
    url: '/filosofo',
    templateUrl: 'templates/filSofo.html',
    controller: 'filSofoCtrl'
  })

  .state('fotGrafo', {
    url: '/fotografo',
    templateUrl: 'templates/fotGrafo.html',
    controller: 'fotGrafoCtrl'
  })

  .state('ingeniero', {
    url: '/ingeniero',
    templateUrl: 'templates/ingeniero.html',
    controller: 'ingenieroCtrl'
  })

  .state('lingIsta', {
    url: '/linguista',
    templateUrl: 'templates/lingIsta.html',
    controller: 'lingIstaCtrl'
  })

  .state('mecNico', {
    url: '/mecanico',
    templateUrl: 'templates/mecNico.html',
    controller: 'mecNicoCtrl'
  })

  .state('mDico', {
    url: '/medico',
    templateUrl: 'templates/mDico.html',
    controller: 'mDicoCtrl'
  })

  .state('mSico', {
    url: '/musico',
    templateUrl: 'templates/mSico.html',
    controller: 'mSicoCtrl'
  })

  .state('odontLogo', {
    url: '/odontologo',
    templateUrl: 'templates/odontLogo.html',
    controller: 'odontLogoCtrl'
  })

  .state('pediatra', {
    url: '/pediatra',
    templateUrl: 'templates/pediatra.html',
    controller: 'pediatraCtrl'
  })

  .state('peluquero', {
    url: '/peluquero',
    templateUrl: 'templates/peluquero.html',
    controller: 'peluqueroCtrl'
  })

  .state('periodista', {
    url: '/periodista',
    templateUrl: 'templates/periodista.html',
    controller: 'periodistaCtrl'
  })

  .state('piloto', {
    url: '/piloto',
    templateUrl: 'templates/piloto.html',
    controller: 'pilotoCtrl'
  })

  .state('pintor', {
    url: '/pintor',
    templateUrl: 'templates/pintor.html',
    controller: 'pintorCtrl'
  })

  .state('plomero', {
    url: '/plomero',
    templateUrl: 'templates/plomero.html',
    controller: 'plomeroCtrl'
  })

  .state('policA', {
    url: '/policia',
    templateUrl: 'templates/policA.html',
    controller: 'policACtrl'
  })

  .state('profesional', {
    url: '/profesional',
    templateUrl: 'templates/profesional.html',
    controller: 'profesionalCtrl'
  })

  .state('psicLogo', {
    url: '/psicologo',
    templateUrl: 'templates/psicLogo.html',
    controller: 'psicLogoCtrl'
  })

  .state('CMo', {
    url: '/cómo',
    templateUrl: 'templates/CMo.html',
    controller: 'CMoCtrl'
  })

  .state('CuL', {
    url: '/cuál',
    templateUrl: 'templates/CuL.html',
    controller: 'CuLCtrl'
  })

  .state('CuNdo', {
    url: '/cuándo',
    templateUrl: 'templates/CuNdo.html',
    controller: 'CuNdoCtrl'
  })

  .state('CuNto', {
    url: '/cuanto',
    templateUrl: 'templates/CuNto.html',
    controller: 'CuNtoCtrl'
  })

  .state('DNde', {
    url: '/donde',
    templateUrl: 'templates/DNde.html',
    controller: 'DNdeCtrl'
  })

  .state('PorQu', {
    url: '/por_qué',
    templateUrl: 'templates/PorQu.html',
    controller: 'PorQuCtrl'
  })

  .state('Qu', {
    url: '/que',
    templateUrl: 'templates/Qu.html',
    controller: 'QuCtrl'
  })

  .state('QuiN', {
    url: '/quien',
    templateUrl: 'templates/QuiN.html',
    controller: 'QuiNCtrl'
  })

  .state('abrir', {
    url: '/abrir',
    templateUrl: 'templates/abrir.html',
    controller: 'abrirCtrl'
  })

  .state('aceptar', {
    url: '/aceptar',
    templateUrl: 'templates/aceptar.html',
    controller: 'aceptarCtrl'
  })

  .state('acompaAr', {
    url: '/acompanar',
    templateUrl: 'templates/acompaAr.html',
    controller: 'acompaArCtrl'
  })

  .state('apoyar', {
    url: '/apoyar',
    templateUrl: 'templates/apoyar.html',
    controller: 'apoyarCtrl'
  })

  .state('asistir', {
    url: '/asistir',
    templateUrl: 'templates/asistir.html',
    controller: 'asistirCtrl'
  })

  .state('ayudar', {
    url: '/ayudar',
    templateUrl: 'templates/ayudar.html',
    controller: 'ayudarCtrl'
  })

  .state('bailar', {
    url: '/bailar',
    templateUrl: 'templates/bailar.html',
    controller: 'bailarCtrl'
  })

  .state('buscar', {
    url: '/buscar',
    templateUrl: 'templates/buscar.html',
    controller: 'buscarCtrl'
  })

  .state('caer', {
    url: '/caer',
    templateUrl: 'templates/caer.html',
    controller: 'caerCtrl'
  })

  .state('cerrar', {
    url: '/cerrar',
    templateUrl: 'templates/cerrar.html',
    controller: 'cerrarCtrl'
  })

  .state('cuidar', {
    url: '/cuidar',
    templateUrl: 'templates/cuidar.html',
    controller: 'cuidarCtrl'
  })

  .state('dar', {
    url: '/dar',
    templateUrl: 'templates/dar.html',
    controller: 'darCtrl'
  })

  .state('empezar', {
    url: '/empezar',
    templateUrl: 'templates/empezar.html',
    controller: 'empezarCtrl'
  })

  .state('encontrar2', {
    url: '/encontrar',
    templateUrl: 'templates/encontrar2.html',
    controller: 'encontrar2Ctrl'
  })

  .state('entrar', {
    url: '/entrar',
    templateUrl: 'templates/entrar.html',
    controller: 'entrarCtrl'
  })

  .state('esperar', {
    url: '/esperar',
    templateUrl: 'templates/esperar.html',
    controller: 'esperarCtrl'
  })

  .state('faltar', {
    url: '/faltar',
    templateUrl: 'templates/faltar.html',
    controller: 'faltarCtrl'
  })

  .state('gustar', {
    url: '/gustar',
    templateUrl: 'templates/gustar.html',
    controller: 'gustarCtrl'
  })

  .state('enero', {
    url: '/enero',
    templateUrl: 'templates/enero.html',
    controller: 'eneroCtrl'
  })

  .state('febrero', {
    url: '/febrero',
    templateUrl: 'templates/febrero.html',
    controller: 'febreroCtrl'
  })

  .state('marzo', {
    url: '/marzo',
    templateUrl: 'templates/marzo.html',
    controller: 'marzoCtrl'
  })

  .state('abril', {
    url: '/abril',
    templateUrl: 'templates/abril.html',
    controller: 'abrilCtrl'
  })

  .state('mayo', {
    url: '/mayo',
    templateUrl: 'templates/mayo.html',
    controller: 'mayoCtrl'
  })

  .state('junio', {
    url: '/junio',
    templateUrl: 'templates/junio.html',
    controller: 'junioCtrl'
  })

  .state('julio', {
    url: '/julio',
    templateUrl: 'templates/julio.html',
    controller: 'julioCtrl'
  })

  .state('agosto', {
    url: '/agosto',
    templateUrl: 'templates/agosto.html',
    controller: 'agostoCtrl'
  })

  .state('septiembre', {
    url: '/septiembre',
    templateUrl: 'templates/septiembre.html',
    controller: 'septiembreCtrl'
  })

  .state('octubre', {
    url: '/octubre',
    templateUrl: 'templates/octubre.html',
    controller: 'octubreCtrl'
  })

  .state('noviembre', {
    url: '/noviembre',
    templateUrl: 'templates/noviembre.html',
    controller: 'noviembreCtrl'
  })

  .state('diciembre', {
    url: '/diciembre',
    templateUrl: 'templates/diciembre.html',
    controller: 'diciembreCtrl'
  })

  .state('ropa', {
    url: '/ropa',
    templateUrl: 'templates/ropa.html',
    controller: 'ropaCtrl'
  })

  .state('abrigo', {
    url: '/abrigo',
    templateUrl: 'templates/abrigo.html',
    controller: 'abrigoCtrl'
  })

  .state('blusa', {
    url: '/blusa',
    templateUrl: 'templates/blusa.html',
    controller: 'blusaCtrl'
  })

  .state('brasier', {
    url: '/brasier',
    templateUrl: 'templates/brasier.html',
    controller: 'brasierCtrl'
  })

  .state('cachucha', {
    url: '/cachucha',
    templateUrl: 'templates/cachucha.html',
    controller: 'cachuchaCtrl'
  })

  .state('camisa', {
    url: '/camisa',
    templateUrl: 'templates/camisa.html',
    controller: 'camisaCtrl'
  })

  .state('camiseta', {
    url: '/camiseta',
    templateUrl: 'templates/camiseta.html',
    controller: 'camisetaCtrl'
  })

  .state('chaqueta', {
    url: '/chaqueta',
    templateUrl: 'templates/chaqueta.html',
    controller: 'chaquetaCtrl'
  })

  .state('cinturon', {
    url: '/cinturon',
    templateUrl: 'templates/cinturon.html',
    controller: 'cinturonCtrl'
  })

  .state('corbata', {
    url: '/corbata',
    templateUrl: 'templates/corbata.html',
    controller: 'corbataCtrl'
  })

  .state('falda', {
    url: '/falda',
    templateUrl: 'templates/falda.html',
    controller: 'faldaCtrl'
  })

  .state('interiores', {
    url: '/interiores',
    templateUrl: 'templates/interiores.html',
    controller: 'interioresCtrl'
  })

  .state('medias', {
    url: '/medias',
    templateUrl: 'templates/medias.html',
    controller: 'mediasCtrl'
  })

  .state('pantalon', {
    url: '/pantalon',
    templateUrl: 'templates/pantalon.html',
    controller: 'pantalonCtrl'
  })

  .state('pantaloncillos', {
    url: '/pantaloncillos',
    templateUrl: 'templates/pantaloncillos.html',
    controller: 'pantaloncillosCtrl'
  })

  .state('pijama', {
    url: '/pijama',
    templateUrl: 'templates/pijama.html',
    controller: 'pijamaCtrl'
  })

  .state('saco', {
    url: '/saco',
    templateUrl: 'templates/saco.html',
    controller: 'sacoCtrl'
  })

  .state('sombrero', {
    url: '/sombrero',
    templateUrl: 'templates/sombrero.html',
    controller: 'sombreroCtrl'
  })

  .state('sudadera', {
    url: '/sudadera',
    templateUrl: 'templates/sudadera.html',
    controller: 'sudaderaCtrl'
  })

  .state('zapato', {
    url: '/zapato',
    templateUrl: 'templates/zapato.html',
    controller: 'zapatoCtrl'
  })

  .state('mam', {
    url: '/mama',
    templateUrl: 'templates/mam.html',
    controller: 'mamCtrl'
  })

  .state('pap', {
    url: '/papa',
    templateUrl: 'templates/pap.html',
    controller: 'papCtrl'
  })

  .state('familia', {
    url: '/familia',
    templateUrl: 'templates/familia.html',
    controller: 'familiaCtrl'
  })

  .state('padres', {
    url: '/padres',
    templateUrl: 'templates/padres.html',
    controller: 'padresCtrl'
  })

  .state('hijo', {
    url: '/hijo',
    templateUrl: 'templates/hijo.html',
    controller: 'hijoCtrl'
  })

  .state('hermano', {
    url: '/hermano',
    templateUrl: 'templates/hermano.html',
    controller: 'hermanoCtrl'
  })

  .state('primo', {
    url: '/primo1',
    templateUrl: 'templates/primo.html',
    controller: 'primoCtrl'
  })

  .state('tO', {
    url: '/tio',
    templateUrl: 'templates/tO.html',
    controller: 'tOCtrl'
  })

  .state('sobrino', {
    url: '/sobrino',
    templateUrl: 'templates/sobrino.html',
    controller: 'sobrinoCtrl'
  })

  .state('abuelo', {
    url: '/abuelo',
    templateUrl: 'templates/abuelo.html',
    controller: 'abueloCtrl'
  })

  .state('nieto', {
    url: '/nieto',
    templateUrl: 'templates/nieto.html',
    controller: 'nietoCtrl'
  })

  .state('primo2', {
    url: '/primo2',
    templateUrl: 'templates/primo2.html',
    controller: 'primo2Ctrl'
  })

  .state('f', {
    url: '/f',
    templateUrl: 'templates/f.html',
    controller: 'fCtrl'
  })

  .state('g', {
    url: '/g',
    templateUrl: 'templates/g.html',
    controller: 'gCtrl'
  })

  .state('j', {
    url: '/j',
    templateUrl: 'templates/j.html',
    controller: 'jCtrl'
  })

  .state('s2', {
    url: '/s',
    templateUrl: 'templates/s2.html',
    controller: 's2Ctrl'
  })

  .state('x', {
    url: '/x',
    templateUrl: 'templates/x.html',
    controller: 'xCtrl'
  })

  .state('y', {
    url: '/y',
    templateUrl: 'templates/y.html',
    controller: 'yCtrl'
  })

  .state('z', {
    url: '/z',
    templateUrl: 'templates/z.html',
    controller: 'zCtrl'
  })

  .state('page251', {
    url: '/n2',
    templateUrl: 'templates/page251.html',
    controller: 'page251Ctrl'
  })

  .state('testMDulo1', {
    url: '/tm1',
    templateUrl: 'templates/testMDulo1.html',
    controller: 'testMDulo1Ctrl'
  })

  .state('testMDulo12', {
    url: '/page259',
    templateUrl: 'templates/testMDulo12.html',
    controller: 'testMDulo12Ctrl'
  })

  .state('testMDulo13', {
    url: '/page255',
    templateUrl: 'templates/testMDulo13.html',
    controller: 'testMDulo13Ctrl'
  })

  .state('testMDulo2', {
    url: '/page265',
    templateUrl: 'templates/testMDulo2.html',
    controller: 'testMDulo2Ctrl'
  })

  .state('testMDulo14', {
    url: '/page256',
    templateUrl: 'templates/testMDulo14.html',
    controller: 'testMDulo14Ctrl'
  })

  .state('testMDulo22', {
    url: '/page266',
    templateUrl: 'templates/testMDulo22.html',
    controller: 'testMDulo22Ctrl'
  })

  .state('testMDulo15', {
    url: '/page260',
    templateUrl: 'templates/testMDulo15.html',
    controller: 'testMDulo15Ctrl'
  })

  .state('testMDulo16', {
    url: '/page262',
    templateUrl: 'templates/testMDulo16.html',
    controller: 'testMDulo16Ctrl'
  })

  .state('fotos', {
    url: '/page299',
    templateUrl: 'templates/fotos.html',
    controller: 'fotosCtrl'
  })

$urlRouterProvider.otherwise('/login')


});